const problem = document.querySelector("#showquestions");
const btnRefresh = document.querySelector(".form");
// const inputAns = document.querySelector(".inputAnswer")
let state = {
  score: 0,
  wrongAnser: 0,
};
function generateNumber(max) {
  return Math.floor(Math.random() * (max + 1));
}

function updateProblem() {
  state.currentProblem = generateProblem();
  problem.innerHTML = `${state.currentProblem.numberOne} ${state.currentProblem.operator} ${state.currentProblem.numberTwo}`;
}
updateProblem()

function generateProblem(max) {
  return {
    numberOne: generateNumber(10),
    numberTwo: generateNumber(10),
    operator: ["+", "-"][generateNumber(1)],
  };
}

//checkbtn
// function checkAns() {
//     var userInput = parseInt(document.getElementById("inputAnswer").value)
//     let correctAns;
//     const p = state.currentProblem;
//     if(p.operator == "+") {
//         correctAns = p.numberOne + p.numberTwo
//         console.log(correctAns)
//     }
//     if(p.operator == "-") {
//         correctAns = p.numberOne - p.numberTwo
//         console.log(correctAns)
//     }
//     if(userInput === correctAns) {
//         openPopup()
//         console.log("Correct!")
//         console.log(userInput)
//     }
//     if(userInput != correctAns) {
//         console.log("try again!")
//         console.log(userInput)
//     }

// }

//resetbtn
function resetPage() {
  location.reload()
}

let correctpopup = document.getElementById("correctpopup");
let wrongpopup = document.getElementById("wrongpopup");
let inputpopup = document.getElementById("inputpopup");



function openPopup() {
  var userInput = parseInt(document.getElementById("inputAnswer").value)
    let correctAns;
    const p = state.currentProblem;
    if(p.operator == "+") {
        correctAns = p.numberOne + p.numberTwo
        console.log(correctAns)
    }
    if(p.operator == "-") {
        correctAns = p.numberOne - p.numberTwo
        console.log(correctAns)
    }
    if(userInput === correctAns) {
      correctpopup.classList.add("open-popup")
    }
    if(userInput != correctAns) {
      wrongpopup.classList.add("open-popup")
    }
    if(userInput = "NaN") {
      inputpopup.classList.add("open-popup")
    }
}

function closePopup() {
  popup.classList.remove("open-popup")
}

